eseguibile_test: main test.o funzioni.o
	g++ -o eseguibile_test test.o funzioni.o

main: main.o funzioni.o
	g++ -o main main.o funzioni.o

main.o: main.cpp funzioni.h
	g++ -c main.cpp

test.o: test.cpp funzioni.h
	g++ -c test.cpp

funzioni.o: funzioni.cpp funzioni.h
	g++ -c funzioni.cpp
